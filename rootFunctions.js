import { Dimensions } from 'react-native'

export function getDimension() {
    const dim = Dimensions.get('screen')
    return { height: dim.height, width: dim.width }
}
