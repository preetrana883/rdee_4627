import React, { Component } from 'react';
import { View, TouchableOpacity, Text, TextInput } from 'react-native';
import {
    AbstractWelcomePage,
    _mapStateToProps as _abstractMapStateToProps
} from '../welcome/components/AbstractWelcomePage';
import { translate } from '../base/i18n';
import { connect } from '../base/redux';
import { updateSettings } from '../base/settings';
import LinearGradient from 'react-native-linear-gradient';


class Video extends AbstractWelcomePage {

    componentDidMount() {
        super.componentDidMount();
        this.setState({ room: 'bhavesh123' })
    }

    onjoin = () => {
        var settings = {
            displayName: 'bhavesh',
        }
        this.props.dispatch(updateSettings(settings));
        this._onJoin()
    }

    render() {
        const hint = 'Hint--> Join button takes you to Conference.js\nPath: react/features/conference/components/native/Conference.js'
        return (
            <LinearGradient
                start={{ x: 0.0, y: 0.25 }} end={{ x: 0.5, y: 1.0 }}
                colors={['#4453D6', '#571B65']} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <TextInput
                    style={{ width: '92%', height: 60, borderWidth: 1, borderColor: 'black', paddingHorizontal: 10 }}
                    onChangeText={(val) => this.setState({ room: val })}
                    value={this.state.room}
                />
                <TouchableOpacity
                    onPress={this.onjoin}
                    style={{ width: 80, height: 40, marginTop: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: 'lightgray' }}>
                    <Text>Join</Text>
                </TouchableOpacity>
                <Text style={{ marginTop: 20, color: 'white', fontSize: 18 }} >Path: react/features/screens/Video.js</Text>
                <Text style={{ marginTop: 20, color: 'white', fontSize: 18, marginHorizontal: 10, textAlign: 'center' }}>{hint}</Text>
            </LinearGradient>
        )
    }
}

function _mapStateToProps(state) {

    return {
        ..._abstractMapStateToProps(state),
        state: state,
    };
}

export default translate(connect(_mapStateToProps)(Video))