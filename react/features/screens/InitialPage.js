import React, { Component } from 'react';
import { View } from 'react-native';
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';
import Chat from './Chat';
import Video from './Video';


const routes = {
    ['Chat']: { screen: Chat },
    ['Video']: { screen: Video },

}

const Navigation = createStackNavigator(
    routes,
    {
        initialRouteName: 'Chat',
        headerMode: 'none',
    })
const Navigation_APP = createAppContainer(Navigation)

export default class InitialPage extends Component {
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'salmon' }}>
                <Navigation_APP />
            </View>
        )
    }
}