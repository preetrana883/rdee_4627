import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';


export default class Chat extends Component {
    render() {
        const serverHint = 'Set your own server assign value to this variable DEFAULT_SERVER_URL\nPath: react/features/base/settings/constants.js'
        const config = 'You can also change the config from here:\nPath: root level -> config.js'
        return (
            <LinearGradient
                start={{ x: 0.0, y: 0.25 }} end={{ x: 0.5, y: 1.0 }}
                colors={['#4453D6', '#571B65']} style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>

                <TouchableOpacity style={{ width: 80, height: 50, backgroundColor: 'lightgray', justifyContent: 'center', alignItems: 'center' }} onPress={() => this.props.navigation.navigate('Video')}>
                    <Text>Video</Text>
                </TouchableOpacity>

                <Text style={{ marginTop: 20, color: 'white', fontSize: 18 }} >Path: react/features/screens/Chat.js</Text>
                <Text style={{ marginTop: 20, color: 'white', fontSize: 18, textAlign: 'center' }} >{serverHint}</Text>
                <Text style={{ marginTop: 20, color: 'white', fontSize: 18, textAlign: 'center' }} >{config}</Text>
            </LinearGradient>
        )
    }
}