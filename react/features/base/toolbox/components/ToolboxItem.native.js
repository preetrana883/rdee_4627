// @flow

import React from 'react';
import { Text, TouchableHighlight, View, TouchableOpacity, Image } from 'react-native';

import { Icon } from '../../icons';

import AbstractToolboxItem from './AbstractToolboxItem';
import type { Props } from './AbstractToolboxItem';
import { ColorPalette } from '../../styles';

/**
 * Native implementation of {@code AbstractToolboxItem}.
 */
export default class ToolboxItem extends AbstractToolboxItem<Props> {
    /**
     * Renders the {@code Icon} part of this {@code ToolboxItem}.
     *
     * @private
     * @returns {ReactElement}
     */
    _renderIcon() {
        const { styles, buttonType, icon, toggled } = this.props;
        var imgIcon = buttonType === 'chat' ? require('../../../../../images/chat.png') : require('../../../../../images/chat.png')
        if (buttonType !== undefined) {
            console.log('toggled-- ', toggled, styles && styles.iconStyle)
            const btnwidth = 50
            var bgColor = ColorPalette.white
            if (buttonType === 'chat') {
                bgColor = ColorPalette.white
            } else if (buttonType === 'audio') {
                bgColor = toggled ? ColorPalette.primaryBlack : ColorPalette.primaryBlue
            } else if (buttonType === 'video') {
                bgColor = toggled ? ColorPalette.primaryBlack : ColorPalette.primaryBlue
            } else if (buttonType === 'hangup') {
                bgColor = ColorPalette.primaryRed
            }

            return (
                <View style={{
                    width: btnwidth, height: btnwidth, justifyContent: 'center', alignItems: 'center', backgroundColor: bgColor,
                    borderWidth: 1, borderColor: bgColor === ColorPalette.primaryBlack ? ColorPalette.white : bgColor,
                    borderRadius: btnwidth / 2, alignSelf: 'center', padding: 8
                }}>
                    <Icon
                        src={this.props.icon}
                        style={[{ fontSize: 28, alignSelf: 'center', color: buttonType === 'chat' ? '#4453D6' : 'white' }]} />
                </View>
            )
        } else {
            return (
                <Icon
                    src={this.props.icon}
                    style={styles && styles.iconStyle} />
            );
        }

    }

    /**
     * Renders this {@code ToolboxItem}. Invoked by {@link AbstractToolboxItem}.
     *
     * @override
     * @protected
     * @returns {ReactElement}
     */
    _renderItem() {
        const {
            disabled,
            elementAfter,
            onClick,
            showLabel,
            styles,
            toggled
        } = this.props;

        let children = this._renderIcon();

        // XXX When using a wrapper View, apply the style to it instead of
        // applying it to the TouchableHighlight.
        let style = styles && styles.style;
        console.log('toolboxitem-style ', style)
        if (showLabel) {
            // XXX TouchableHighlight requires 1 child. If there's a need to
            // show both the icon and the label, then these two need to be
            // wrapped in a View.
            children = (
                <View style={[style, { marginLeft: -20 }]}>
                    {/* {children} */}
                    <Text style={[styles && styles.labelStyle, { color: 'white', fontSize: 18, fontWeight: '500', }]}>
                        {this.label}
                    </Text>
                    {elementAfter}
                </View>
            );

            // XXX As stated earlier, the style was applied to the wrapper View
            // (above).
            style = undefined;
        }

        // return (
        //     <TouchableHighlight
        //         accessibilityLabel={this.accessibilityLabel}
        //         accessibilityRole='button'
        //         accessibilityState={{ 'selected': toggled }}
        //         disabled={disabled}
        //         onPress={onClick}
        //         style={style}
        //         underlayColor={styles && styles.underlayColor} >
        //         {children}
        //     </TouchableHighlight>
        // );

        return (
            <TouchableOpacity
                accessibilityLabel={this.accessibilityLabel}
                accessibilityRole='button'
                accessibilityState={{ 'selected': toggled }}
                disabled={disabled}
                onPress={onClick}
                style={style}
                underlayColor={styles && styles.underlayColor} >
                {children}
            </TouchableOpacity>
        );

    }
}
