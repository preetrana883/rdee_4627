// @flow

import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet, TextInput } from 'react-native';
import type { Dispatch } from 'redux';

import { InputDialog } from '../../base/dialog';
import { connect } from '../../base/redux';

import { endRoomLockRequest } from '../actions';
import LinearGradient from 'react-native-linear-gradient';
import { getDimension } from '../../../../rootFunctions';
import { ColorPalette } from '../../base/styles';

/**
 * The style of the {@link TextInput} rendered by {@code RoomLockPrompt}. As it
 * requests the entry of a password, {@code TextInput} automatically correcting
 * the entry of the password is a pain to deal with as a user.
 */
const _TEXT_INPUT_PROPS = {
    autoCapitalize: 'none',
    autoCorrect: false
};

/**
 * The type of the React {@code Component} props of {@link RoomLockPrompt}.
 */
type Props = {

    /**
     * The JitsiConference which requires a password.
     */
    conference: Object,

    /**
     * The number of digits to be used in the password.
     */
    passwordNumberOfDigits: ?number,

    /**
     * Redux store dispatch function.
     */
    dispatch: Dispatch<any>
};

/**
 * Implements a React Component which prompts the user for a password to lock  a
 * conference/room.
 */
class RoomLockPrompt extends Component<Props> {
    /**
     * Initializes a new RoomLockPrompt instance.
     *
     * @param {Props} props - The read-only properties with which the new
     * instance is to be initialized.
     */
    constructor(props: Props) {
        super(props);

        // Bind event handlers so they are only bound once per instance.
        this._onCancel = this._onCancel.bind(this);
        this._onSubmit = this._onSubmit.bind(this);
        this._validateInput = this._validateInput.bind(this);

        this.state = {
            roomPassword: ''
        }

    }

    /**
     * Implements React's {@link Component#render()}.
     *
     * @inheritdoc
     * @returns {ReactElement}
     */
    render() {
        let textInputProps = _TEXT_INPUT_PROPS;

        if (this.props.passwordNumberOfDigits) {
            textInputProps = {
                ...textInputProps,
                keyboardType: 'number-pad',
                maxLength: this.props.passwordNumberOfDigits
            };
        }

        return (
            <View style={{ flex: 1, ...StyleSheet.absoluteFillObject, justifyContent: 'center' }}>
                <LinearGradient
                    start={{ x: 0.0, y: 0.25 }} end={{ x: 0.5, y: 1.0 }}
                    style={{ flex: 1, justifyContent: 'center', alignItems: 'center', opacity: 0.7 }} colors={ColorPalette.primaryGradient} />

                {/* input view */}
                <View style={{ position: 'absolute', opacity: 1, left: 40, right: 40, width: getDimension().width - 80, borderRadius: 30, marginHorizontal: 10, backgroundColor: ColorPalette.modalBg, alignItems: 'center', }}>
                    <Text style={{ color: 'white', fontSize: 18, width: '90%', marginVertical: 20, marginTop: 30, paddingLeft: 10 }}>Set meeting password</Text>
                    <TextInput
                        style={localStyles.textInput}
                        secureTextEntry={true}
                        onChangeText={(val) => this.setState({ roomPassword: val })}
                    />
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '90%', marginBottom: 30 }}>
                        <TouchableOpacity style={localStyles.button} onPress={this._onCancel} >
                            <Text style={localStyles.btnText}>Cancel</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[localStyles.button, { backgroundColor: '#434FD9' }]} onPress={() => this._onSubmit(this.state.roomPassword)} >
                            <Text style={[localStyles.btnText, { color: 'white' }]}>Confirm</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        )

        // return (
        //     <InputDialog
        //         contentKey='dialog.passwordLabel'
        //         onCancel={this._onCancel}
        //         onSubmit={this._onSubmit}
        //         textInputProps={textInputProps}
        //         validateInput={this._validateInput} />
        // );
    }

    _onCancel: () => boolean;

    /**
     * Notifies this prompt that it has been dismissed by cancel.
     *
     * @private
     * @returns {boolean} True to hide this dialog/prompt; otherwise, false.
     */
    _onCancel() {
        // An undefined password is understood to cancel the request to lock the
        // conference/room.
        return this._onSubmit(undefined);
    }

    _onSubmit: (?string) => boolean;

    /**
     * Notifies this prompt that it has been dismissed by submitting a specific
     * value.
     *
     * @param {string|undefined} value - The submitted value.
     * @private
     * @returns {boolean} False because we do not want to hide this
     * dialog/prompt as the hiding will be handled inside endRoomLockRequest
     * after setting the password is resolved.
     */
    _onSubmit(value: ?string) {
        this.props.dispatch(endRoomLockRequest(this.props.conference, value));

        return false; // Do not hide.
    }

    _validateInput: (string) => boolean;

    /**
     * Verifies input in case only digits are required.
     *
     * @param {string|undefined} value - The submitted value.
     * @private
     * @returns {boolean} False when the value is not valid and True otherwise.
     */
    _validateInput(value: string) {

        // we want only digits, but both number-pad and numeric add ',' and '.' as symbols
        if (this.props.passwordNumberOfDigits
            && value.length > 0
            && !/^\d+$/.test(value)) {

            return false;
        }

        return true;
    }
}

const localStyles = StyleSheet.create({
    button: {
        width: '47%', height: 50, backgroundColor: 'white', borderRadius: 25, justifyContent: 'center',
        alignItems: 'center'
    },
    textInput: {
        height: 50, width: '90%', backgroundColor: 'rgba(0, 0, 0, 0.5)',
        borderRadius: 20, marginBottom: 20, color: '#FFFFFF', paddingHorizontal: 15
    },
    btnText: {
        color: 'black',
        fontSize: 18
    }
})

export default connect()(RoomLockPrompt);
